
# Calculador Pi: Uso básico

## Ejecutar el programa
Para ejecutar el programa mediante el uso de consola, debes realizar los siguientes pasos

1. Estar dentro del directorio **calculadorPi**.
2. Usar el comando **make jar**, para crear el archivo que vamos a ejecutar.
3. Finalmente utilize el comando **java -jar pi.jar *numero* ** donde *numero* es un numero entero, que, cuanto mayor sea, más precisa la aproximación a pi.


# Calculador Pi: Uso avanzado

## El javadoc
El javadoc es una herramienta que nos permite generar un documento que contiene información detallada sobre el programa, para crearlo, y leerlo, sigue los siguientes pasos


1. Estar dentro del directorio **calculadorPi**
2. Usar el comando **make doc**, para generar el documento.
3. Usar el comando **firefox html/index.html &** para visualizarlo.

## Visualización gráfica

